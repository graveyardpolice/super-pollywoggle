﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	#region Global Variables
	public static GameObject[] playersToSpawn = new GameObject[4]; //globally stores our list of players joining the match

	public static int[] playerControllers = new int[4];	//globally stores the controllers that each player will use, so we can assign them properly

	public static GameMode _gameMode;
	public static int[] playerLives = new int[4];
	public static int numOfLives;
	public static int playersAlive;
	public static bool isScreenShaking;

	public class KillTracker 
	{
		public int[] player1 = new int[4];
		public int[] player2 = new int[4];
		public int[] player3 = new int[4];
		public int[] player4 = new int[4];
	}

	public static KillTracker killTracker = new KillTracker();

	#endregion
	public enum GameMode
	{
		FFADeathMatch
	}
	// Use this for initialization
	void Start () 
	{

	}

	public static void SetUpGame(int numOfPlayers)
	{
		switch(_gameMode)
		{
		case GameMode.FFADeathMatch:
			for(int i = 0; i < numOfPlayers; i++)
			{
				if(playersToSpawn[i] != null)
				{
					playerLives[i] = numOfLives;
				}
			}
			break;
		}
	}

	public static void  AddKills(int playerKilling, int playerKilled)
	{
		var i = playerKilled - 1;
		switch(playerKilling)
		{
		case 1:
			killTracker.player1[i]++;
			break;
		case 2:
			killTracker.player2[i]++;
			break;
		case 3:
			killTracker.player3[i]++;
			break;
		case 4:
			killTracker.player4[i]++;
			break;
		}



	}
	
	public static void RemoveLives(int playerKilled)
	{
		var i = playerKilled - 1;
		playerLives[i]--;
	}

	public static int CheckLives(int playerNum)
	{
		var i = playerNum - 1;
		return playerLives[i];
	}

	//TODO: FIX THIS!! new inputs broke my old setup
	public static void ResetGame()
	{
		playersAlive = 0;
		//playerLives = new List<int>();
		/**for(int i = 0; i < playersToSpawn.Count; i++)
		{
			if(playersToSpawn.Count >= 1) killTracker.player1[i] = 0;
			if(playersToSpawn.Count >= 2) killTracker.player2[i] = 0;
			if(playersToSpawn.Count >= 3) killTracker.player3[i] = 0;
			if(playersToSpawn.Count == 4) killTracker.player4[i] = 0;
		}**/
	}
}
