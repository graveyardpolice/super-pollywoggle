﻿//using UnityEngine;
//using System.Collections;
//
//public class PlayerSelection : MonoBehaviour {
//	#region Public Variables
//	public int curButton;	//store our current active button for controller/keyboard navigation
//	public GameObject[] prefabs = new GameObject[4]; //stores our gameobjects that players can pick from to play as
//	public GUISkin skin;	//our GUI look is defined by this skin
//	public float padding;	//spacing between GUI elements
//	//TODO: figure out something to do with this variable below
//	public Texture2D[] characters = new Texture2D[4];	//placeholder for when we get textures to use on the character selection screen
//	public Texture2D background;
//	public Texture2D splashLogo;
//	private Color col;
//	public AudioClip selectOption, navigate, back;
//	private AudioSource _audio;
//
//	public class GUIButton	//class to hold information about our menu buttons
//	{
//		public string controlName;
//		public Rect rect;
//		public string text;
//	}
//	#endregion
//	#region Private Variables
//	private string[] strArray = new string[4]; //array of text to show for each player on the player select screen, located at bottom
//	private GUIButton[] buttons = new GUIButton[3]; //array to hold all of our menu options
//	private string[] optionNames = new string[3]; // array to hold the name and controlname of each button
//	private Rect[] buttonRect = new Rect[3]; //defines positioning and shape of our buttons
//	private int maxPlayers = 4; //maximum allowed players
//	private int curPlayers;	//current players in game
//
//	//cache some of our script specific information 
//	private int _screenHeight, _screenWidth;
//	private MenuState _curState;
//	private Color origbg;
//	private enum MenuState 	//state machine for our GUI
//	{
//		Start,
//		MainMenu,
//		PlayerSelect,
//		Options,
//		GameSetup,
//		Exit,
//		Splash
//	}
//	#endregion
//
//	// Use this for initialization
//	void Start () 
//	{
//		_audio = GetComponent<AudioSource>();
//		origbg = Camera.main.backgroundColor;
//		col = Color.white;
//		//Set up our menu string array 
//		//TODO: find a better way to do this
//		optionNames[0] = "Player Select";
//		optionNames[1] = "Options";
//		optionNames[2] = "Exit";
//
//		//cache our variables
//		_curState = MenuState.Splash;
//		_screenHeight = Screen.height;
//		_screenWidth = Screen.width;
//
//		//Setup our string array for our player selection screen
//		for(int i = 0; i < strArray.Length; i++)
//		{
//			strArray[i] = "Press "  + cInput.GetText("Jump P" + i, 1) + " or A To Join";
//		}
//		//set our current players to 0 incase it isnt
//		curPlayers = 0;
//		//setup our buttons for our menu
//		SetupButtons();
//	}
//	
//	// Update is called once per frame
//	void Update () 
//	{
//		//menu state machine
//		switch(_curState)
//		{
//		case MenuState.Splash:
//			Camera.main.backgroundColor = Color.black;
//			col.a -= Time.deltaTime * .5f;
//			if(col.a <=0)
//			{
//				Camera.main.backgroundColor = origbg;
//				_curState = MenuState.Start;
//				col.a = 1;
//			}
//			break;
//		case MenuState.Start:
//			//loop through controllers that are plugged in and scan for input
//			//if we recieve input set it as our controller to use for menu navigation and change state to menu
//			for(int i = 0; i < maxPlayers; i++)
//			{
//				if(cInput.GetKeyDown("Start P" + i))
//				{
//					_audio.PlayOneShot(selectOption);
//					_curState = MenuState.MainMenu;
//				}
//			}
//			break;
//		case MenuState.MainMenu:
//			for(int i = 0; i < maxPlayers; i++)
//			{
//				//Navigation
//				if(cInput.GetKeyDown("Up P" + i))
//				{
//					_audio.PlayOneShot(navigate);
//					if(curButton > 0) curButton--;
//					else curButton = buttons.Length -1;
//				}
//				if(cInput.GetKeyDown("Down P" + i))
//				{
//					_audio.PlayOneShot(navigate);
//					if(curButton < buttons.Length -1) curButton++;
//					else curButton = 0;
//				}
//				//Select option
//				if(cInput.GetKeyDown("Jump P" + i))
//				{
//					_audio.PlayOneShot(selectOption);
//					if(buttons[curButton].controlName == optionNames[0]) _curState = MenuState.PlayerSelect;
//					//else if(buttons[curButton].controlName == optionNames[1]) _curState = MenuState.Options;
//					else if(buttons[curButton].controlName == optionNames[2]) _curState = MenuState.Exit;
//				}
//			}
//			break;
//		case MenuState.Options:
//			//TODO: setup options menu
//			break;
//		case MenuState.PlayerSelect:
//			//loop through our controllers again and scan for input
//			//if we find input assign that gamepad to the first open player position
//			if(cInput.GetKeyDown("Jump P0"))
//			{
//				_audio.PlayOneShot(selectOption);
//				strArray[0] = "Press Start or Enter to Begin";
//				curPlayers++;
//				if(GameManager.playerControllers[0] == 0) GameManager.playerControllers[0] = 0;
//				if(GameManager.playersToSpawn[0] == null)
//				{
//					GameManager.playersToSpawn[0] = prefabs[0];
//				}
//			}
//			if(cInput.GetKeyDown("Jump P1"))
//			{
//				_audio.PlayOneShot(selectOption);
//				curPlayers++;
//				strArray[1] = "Press Start or Enter to Begin";
//				if(GameManager.playerControllers[1] == 0) GameManager.playerControllers[1] = 1;
//				if(GameManager.playersToSpawn[1] == null) GameManager.playersToSpawn[1] = prefabs[1];
//			}
//			if(cInput.GetKeyDown("Jump P2"))
//			{
//				_audio.PlayOneShot(selectOption);
//				curPlayers++;
//				strArray[2] = "Press Start or Enter to Begin";
//				if(GameManager.playerControllers[2] == 0) GameManager.playerControllers[2] = 2;
//				if(GameManager.playersToSpawn[2] == null) GameManager.playersToSpawn[2] = prefabs[2];
//			}
//			if(cInput.GetKeyDown("Jump P3"))
//			{
//				_audio.PlayOneShot(selectOption);
//				curPlayers++;
//				strArray[3] = "Press Start or Enter to Begin";
//				if(GameManager.playerControllers[3] == 0) GameManager.playerControllers[3] = 3;
//				if(GameManager.playersToSpawn[3] == null) GameManager.playersToSpawn[3] = prefabs[3];
//			}
//			for(int i = 0; i < maxPlayers; i++)
//			{
//				if(cInput.GetKeyDown("Back P" + i))
//				{
//					_audio.PlayOneShot(back);
//					_curState = MenuState.MainMenu;
//				}
//				//if we have one or more players we can start the game
//				//TODO: possibly make it require more than one player, but for now it is fine for testing
//				if(curPlayers > 0 && cInput.GetKeyDown("Start P" + i))
//				{
//					_curState = MenuState.GameSetup;
//				}
//			}
//			break;
//		case MenuState.GameSetup:
//			//TODO: setup Game Setup menu
//			//Temporary bypass until Game Manager is finished
//			Application.LoadLevel("test");
//			break;
//		case MenuState.Exit:
//			//TODO: setup exit confirmation menu
//			Application.Quit();
//			break;
//		}
//	}
//
//	//TODO: make it not so ugly, and optimize
//	void OnGUI()
//	{
//		//set our skin for our GUI
//
//		if(_curState != MenuState.Splash)
//		{
//			GUI.skin = skin;
//			GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), background, ScaleMode.ScaleAndCrop);
//		}
//		//state machine
//		switch(_curState)
//		{
//		case MenuState.Splash:
//			GUI.color = col;
//			GUI.DrawTexture(new Rect(0,0, _screenWidth, _screenHeight), splashLogo, ScaleMode.ScaleToFit);
//			break;
//		case MenuState.Start:
//			//TODO: add logo and such
//			//show a screen asking for input, so we can determine which controller will work on the menu
//			GUI.Label(new Rect(padding, padding, _screenWidth - (padding * 2), _screenHeight - (padding * 2)), "This game is currently in the early stages of development. Please be prepared for bugs, placeholder art and assets, balancing issues, and anything else you could possibly expect from an unfinished game! \n \n \n Press Start or Enter to Begin");
//			break;
//		case MenuState.MainMenu:
//			GUI.Label(new Rect(0, padding, _screenWidth, _screenHeight/3), "SUPER POLLYWOGGLE");
//			//loop through our buttons and make them navigable by keyboard/controller
//			foreach(GUIButton button in buttons)
//			{
//				GUI.SetNextControlName(button.controlName);
//				if(GUI.Button(button.rect, button.text))
//				{
//					if(button.controlName == optionNames[0]) _curState = MenuState.PlayerSelect;
//					else if(button.controlName == optionNames[1]) _curState = MenuState.Options;
//					else if(button.controlName == optionNames[2]) _curState = MenuState.Exit;
//				}
//			}
//			//focus our GUI on our current highlighted button
//			GUI.FocusControl(buttons[curButton].controlName);
//			break;
//		case MenuState.Options:
//			//TODO: create an options menu
//			break;
//		case MenuState.PlayerSelect:
//			//create 4 boxes for each player to select a character from
//			//TODO: add textures and allow players to pick which character they can play as instead of auto assigning each player to a specific one
//			//player 1
//			GUI.Box(new Rect(0 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 1");
//			GUI.Label(new Rect(0 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), strArray[0]);
//			//player 2
//			GUI.Box(new Rect(1 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 2");
//			GUI.Label(new Rect(1 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), strArray[1]);
//			//player 3
//			GUI.Box(new Rect(2 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 3");
//			GUI.Label(new Rect(2 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), strArray[2]);
//			//player 4
//			GUI.Box(new Rect(3 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 4");
//			GUI.Label(new Rect(3 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), strArray[3]);
//			break;
//		case MenuState.GameSetup:
//			//TODO: setup Game Setup menu
//			break;
//		case MenuState.Exit:
//			//TODO: setup exit confirmation menu
//			break;
//		}
//
//	}
//	//setup our buttons before we send them to the GUI
//	private void SetupButtons()
//	{
//		for(int i = 0; i < buttonRect.Length; i++)
//		{
//			buttonRect[i] = new Rect((_screenWidth/2) - (_screenWidth/8), ((_screenHeight/5) * i + padding) + _screenHeight/3, _screenWidth/4 - (padding * 2), _screenHeight/5 - (padding * 2)); 
//			buttons[i] = new GUIButton();
//			buttons[i].controlName = optionNames[i];
//			buttons[i].rect = buttonRect[i];
//			buttons[i].text = optionNames[i];
//		} 
//	}
//}
