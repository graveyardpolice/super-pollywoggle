//using UnityEngine;
//using System.Collections;
//
//public class InpCon : MonoBehaviour {
//
//#region Internal Types
//	private struct CharacterRaycastOrigins //stores our origins for our raycasts
//	{
//		public Vector3 topRight;
//		public Vector3 topLeft;
//		public Vector3 bottomRight;
//		public Vector3 bottomLeft;
//	}
//
//
//	//different states for the player to be in
//	public enum PlayerState 
//	{
//		Controllable,
//		Dodge,
//		AirDodge,
//		Stomp,
//		KnockBack,
//		Stomped,
//		DashedUp,
//		UpDash,
//		Dead,
//		Respawn
//	}
//#endregion
//
//
//#region Variables and Declarations
//	//Public Variables
//	//Each of these should be adjusted in the inspector
//	//Adjusts movement and delays
//	public float gravity, runSpeed, groundDamping, jumpHeight, 
//				normalizedHorizontalSpeed, stompPower, stompPause, 
//				knockPower, knockRecovery, respawnTime, knockbackPower, dashTimer, dashPower;
//	//Sets our player number, so we know which player is controlling this character
//	public int playerNum;
//
//	//Audio clips for playing sounds at events
//	public AudioClip impact;
//	public AudioClip[] dash = new AudioClip[4];
//
//	//adjusting this changes the accuracy of the ray detections
//	[Range( 2, 20 )]
//	public int totalHorizontalRays = 8;
//	[Range( 2, 20 )]
//	public int totalVerticalRays = 4;
//	
//	//stores our animation clips so we can know the length of them
//	public AnimationClip dodge, respawn;
//
//	public AudioClip[] VA = new AudioClip[9];
//	public AudioClip[] VR = new AudioClip[9];
//
//	private AudioSource _vaAudio;
//	private AudioSource _vrAudio;
//
//	//stores the position to layer the dust cloud effects at
//	public GameObject dashPuffpos;
//	public GameObject[] dashPuffPool = new GameObject[2];
//	public int dashPuffIndex;
//	public GameObject contactPos;
//
//	//Tracker for offscreen
//	public GameObject tracker;
//	public AudioClip footstep, death, respawnSound, jump;
//	//private Variables
//	public bool AirDodged, hit, upDash, hasUpDashed, hasJumped, playSpawn;
//
//	//our layermask for things we can collide with
//	[SerializeField]
//	private LayerMask playerMask;
//	private bool stompSound;
//	//locally cached variables
//	private float _timer; //variable to hold a timer that gets counted down when needed
//	private CharacterController2D _controller;
//	public Animator _animator;
//	private Vector3 _velocity;
//	public PlayerState _currentState;
//	public BoxCollider2D _box;
//	private SpawnManager _gm;
//	private CharacterRaycastOrigins _raycastOrigins;
//	private float _verticalDistanceBetweenRays, _horizontalDistanceBetweenRays, _skinWidth; //variables storing information about our raycasts
//	private RaycastHit2D _raycastHit;
//	private AudioSource _audio;
//	private CameraShake _camShake;
//	private Animator[] _puffAnim;
//	private Animator _stompPart;
//	public int lastHitBy;
//	private Animator _contact;
//
//#endregion
//#region Awake and Start
//	void Awake()
//	{
//		_vaAudio = transform.FindChild("Action").GetComponent<AudioSource>();
//		_vrAudio = transform.FindChild("React").GetComponent<AudioSource>();
//		_puffAnim = new Animator[dashPuffPool.Length];
//		_stompPart = transform.FindChild("Particles").GetComponent<Animator>();
//		for(int i = 0; i < dashPuffPool.Length; i++)
//		{
//			dashPuffPool[i] = (GameObject)Instantiate(dashPuffpos);
//			dashPuffPool[i].hideFlags = HideFlags.HideInHierarchy;
//			dashPuffPool[i].transform.parent = null;
//			_puffAnim[i] = dashPuffPool[i].GetComponent<Animator>();
//		}
//		_audio = GetComponent<AudioSource>();
//		_controller = GetComponent<CharacterController2D>();
//		_animator = GetComponent<Animator>();
//		_box = GetComponent<BoxCollider2D>();
//		_gm = GameObject.Find("GameManager").GetComponent<SpawnManager>();
//		_camShake = GetComponent<CameraShake>();
//		contactPos = (GameObject)Instantiate(contactPos);
//		contactPos.transform.parent = null;
//		_contact = contactPos.GetComponent<Animator>();
//	}
//
//	// Use this for initialization
//	void Start () 
//	{
//		tracker = (GameObject)Instantiate(tracker);
//		ScreenTracker st = tracker.GetComponent<ScreenTracker>();
//		st.SetTracker(gameObject);
//
//		//set some of our variables in case they are wrong
//		_timer = 0;
//		_currentState = PlayerState.Respawn;
//		_skinWidth = _controller.skinWidth;
//		dashPuffIndex = 0;
//
//		//calculate our distance between rays
//		recalculateDistanceBetweenRays();
//		_animator.SetBool("Respawn", true);
//		_audio.PlayOneShot(respawnSound);
//	}
//#endregion
//	// Update is called once per frame
//	void Update () 
//	{
//		//grab our current velocity for calculations
//		_velocity = _controller.velocity;
//		_animator.SetFloat("yVel", _velocity.y);
//		//if we are grounded tell our animator we are.
//		_animator.SetBool("Grounded", _controller.isGrounded);
//		//reset our airdodge if we hit the ground and have air dodged
//		if(_controller.isGrounded)
//		{ 
//			AirDodged = false;
//			upDash = false;
//			hasJumped = false;
//			hasUpDashed = false;
//		}
//		if(!_controller.isGrounded && !upDash && !hasJumped) upDash = true;
//
//	
//		#region FSM
//		switch(_currentState)
//		{
//			case PlayerState.Controllable:
//			ResetAnim();
//			//flip our sprite to match our direction if it needs flipped
//			if(cInput.GetAxisRaw("Horizontal P" + playerNum.ToString()) < 0 && transform.localScale.x > 0)
//				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z);
//			else if(cInput.GetAxisRaw("Horizontal P" + playerNum.ToString()) > 0 && transform.localScale.x < 0) 
//				transform.localScale = new Vector3( -transform.localScale.x, transform.localScale.y, transform.localScale.z);
//
//
//			//our normalized speed is equal to our keyboardSpeed equation or our thumbstick if we are using a gamepad
//			normalizedHorizontalSpeed += cInput.GetAxisRaw("Horizontal P" + playerNum.ToString()) * 2.5f * Time.deltaTime;
//			if(cInput.GetAxisRaw("Horizontal P" + playerNum.ToString()) == 0)
//			{
//				if(transform.localScale.x == 1)
//				{
//					normalizedHorizontalSpeed = normalizedHorizontalSpeed > 0 ? normalizedHorizontalSpeed - (2.5f * Time.deltaTime) : 0;
//				}
//				if(transform.localScale.x == -1)
//				{
//					normalizedHorizontalSpeed = normalizedHorizontalSpeed < 0 ? normalizedHorizontalSpeed + (2.5f * Time.deltaTime) : 0;
//				}
//			}
//			//make sure we dont go over 1, if we do, set it to match the direction we are travelling
//			if(Mathf.Abs(normalizedHorizontalSpeed) > 1)
//				normalizedHorizontalSpeed = transform.localScale.x;
//
//			//check for inputs and apply forces or swap states accordingly
//			if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && !_controller.isGrounded && cInput.GetAxisRaw("Vertical P" + playerNum.ToString()) < 0)
//			{
//				_vaAudio.PlayOneShot(VA[Random.Range(0, VA.Length)]);
//				_currentState = PlayerState.Stomp;
//				PlayDashParticles();
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//			} 
//			else if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && upDash && !_controller.isGrounded)
//			{
//				_vaAudio.PlayOneShot(VA[Random.Range(0, VA.Length)]);
//				audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_currentState = PlayerState.UpDash;
//				PlayDashParticles();
//			}
//			if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && _controller.isGrounded)
//			{
//				_vaAudio.PlayOneShot(VA[Random.Range(0, VA.Length)]);
//				_audio.PlayOneShot(jump);
//				_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );
//				upDash = true;
//				hasJumped = true;
//			} 
//			if(cInput.GetKeyUp("Jump P" + playerNum.ToString()) && !_controller.isGrounded && _velocity.y > 0 && !hasUpDashed)
//				_velocity.y = 0;
//			if(cInput.GetKeyDown("Dodge P" + playerNum.ToString()))
//			{ 
//				if(_controller.isGrounded)
//				{
//					_vaAudio.PlayOneShot(VA[Random.Range(0, VA.Length)]);
//					PlayDashParticles();
//					_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//					_currentState = PlayerState.Dodge;
//					normalizedHorizontalSpeed = 2 * transform.localScale.x;
//				}
//				else if(!_controller.isGrounded && !AirDodged)
//				{
//					_vaAudio.PlayOneShot(VA[Random.Range(0, VA.Length)]);
//					PlayDashParticles();
//					_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//					_currentState = PlayerState.AirDodge;
//					normalizedHorizontalSpeed = 2.75f * transform.localScale.x;
//				}
//					
//			}
//			break;
//		case PlayerState.Dodge:
//			Dodge ();
//			DodgeCollisions();
//			break;
//		case PlayerState.AirDodge:
//			DodgeCollisions();
//			AirDodge();
//			break;
//		case PlayerState.UpDash:
//			UpDash();
//			DodgeCollisions();
//			break;
//		case PlayerState.Stomp:
//			Stomp ();
//			StompCollisions();
//			break;
//		case PlayerState.Stomped:
//			StompKnockBack();
//			break;
//		case PlayerState.DashedUp:
//			StompKnockBack();
//			break;
//		case PlayerState.KnockBack:
//			KnockBack(knockbackPower);
//			break;
//		case PlayerState.Dead:
//			Die();
//			break;
//		case PlayerState.Respawn:
//			_animator.SetBool("Respawn", true);
//			Respawn();
//			break;
//		}
//		#endregion
//		//set our speed in our animator to be equal to our normalized speed
//		_animator.SetFloat("Speed", Mathf.Abs(normalizedHorizontalSpeed));
//
//				
//		// apply gravity before moving
//		if(_currentState != PlayerState.AirDodge && _currentState != PlayerState.Respawn)_velocity.y += gravity * Time.deltaTime;
//
//		//send our movements to our controller
//		_controller.move( _velocity * Time.deltaTime );
//	}
//	public void PlayFootstep()
//	{
//		_audio.PlayOneShot(footstep);
//	}
//
//	private void PlayDashParticles()
//	{
//		GameObject go = dashPuffPool[dashPuffIndex];
//		switch(_currentState)
//		{
//		case PlayerState.Stomp:
//			go.transform.position = new Vector3(transform.position.x, transform.position.y + 1.6f, transform.position.z);
//			go.transform.rotation = Quaternion.Euler(0,0,-90);
//			go.transform.localScale = new Vector3(1, 1, 1);
//			_puffAnim[dashPuffIndex].Play("dashPuff");
//			break;
//		case PlayerState.UpDash:
//			go.transform.position = new Vector3(transform.position.x, transform.position.y - 1.6f, transform.position.z);
//			go.transform.rotation = Quaternion.Euler(0,0,90);
//			go.transform.localScale = new Vector3(1, 1, 1);
//			_puffAnim[dashPuffIndex].Play("dashPuff");
//			break;
//		default:
//			go.transform.position = new Vector3(transform.position.x - (1.6f * transform.localScale.x), transform.position.y, transform.position.z);
//			go.transform.localScale = transform.localScale;
//			go.transform.rotation = Quaternion.Euler(0,0,0);
//			_puffAnim[dashPuffIndex].Play("dashPuff");
//			break;
//		}
//		dashPuffIndex = dashPuffIndex >= dashPuffPool.Length - 1 ? 0 : dashPuffIndex + 1;
//
//	}
//
//	private void PlayContact()
//	{
//		switch(_currentState)
//		{
//		case PlayerState.DashedUp:
//			contactPos.transform.position = new Vector3(transform.position.x, transform.position.y - 1.6f, transform.position.z);
//			break;
//		case PlayerState.KnockBack:
//			var hitdir = knockbackPower < 0 ? -1 : 1;
//			contactPos.transform.position = new Vector3(transform.position.x - (1.6f * hitdir), transform.position.y, transform.position.z);
//			break;
//		case PlayerState.Stomped:
//			contactPos.transform.position = new Vector3(transform.position.x, transform.position.y + 1.6f, transform.position.z);
//			break;
//		}
//		_contact.Play("Contact");
//	}
//	/// <summary>
//	/// Handles our Respawn player state.
//	/// </summary>
//	private void Respawn()
//	{
//		if(_timer <= respawn.length)
//		{
//			_box.enabled = false;
//			normalizedHorizontalSpeed = 0;
//			_velocity.y = 0;
//			_timer += Time.deltaTime;
//		}
//		else
//		{
//			_box.enabled = true;
//			playSpawn = false;
//			_animator.SetBool("Respawn", false);
//			_timer = 0;
//			_box.enabled = true;
//			_currentState = PlayerState.Controllable;
//		}
//	}
//
//	/// <summary>
//	/// Handles our player UpDash State.
//	/// </summary>
//	private void UpDash()
//	{
//		if(!hasUpDashed)
//		{
//			if(!hasJumped) hasJumped = true;
//			upDash = false;
//			hasUpDashed = true;
//			_velocity.y = dashPower;
//			_animator.SetBool("UpDash", true);
//		}
//		if(hasJumped && _timer <= dashTimer)
//		{
//			normalizedHorizontalSpeed = 0;
//			_timer += Time.deltaTime;
//			_velocity.y -= dashPower * Time.deltaTime * 2;
//			if(cInput.GetKeyDown("Dodge P" + playerNum.ToString()) && !AirDodged)
//			{
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_animator.SetBool("UpDash", false);
//				normalizedHorizontalSpeed = 3f * transform.localScale.x;
//				_currentState = PlayerState.AirDodge;
//				PlayDashParticles();
//				_timer = 0;
//			}
//		}
//		else 
//		{
//			_animator.SetBool("UpDash", false);
//			_currentState = PlayerState.Controllable;
//			_timer = 0;
//		}
//	}
//
//	private void StompCollisions()
//	{
//		if(CheckVertical())
//		{
//			if(_raycastHit.collider != null)
//			{
//				if(_raycastHit.collider.tag == "Players")
//				{
//					GameObject playerHit = _raycastHit.collider.gameObject;
//					InpCon _playerController = playerHit.GetComponent<InpCon>();
//					_playerController.lastHitBy = playerNum;
//					_camShake.ShakeCamera(.05f, true);
//					if(_playerController._currentState != PlayerState.Stomped) audio.PlayOneShot(impact);
//					_playerController._currentState = PlayerState.Stomped;
//				}
//			}
//		}
//	}
//	private void DodgeCollisions()
//	{
//		if(CheckSides() && _currentState == PlayerState.Dodge || _currentState == PlayerState.AirDodge)
//		{
//			if(_raycastHit.collider != null)
//			{
//				if(_raycastHit.collider.tag == "Players")
//				{
//					GameObject playerHit = _raycastHit.collider.gameObject;
//					InpCon _playerController = playerHit.GetComponent<InpCon>();
//					if(_playerController._currentState != PlayerState.KnockBack)
//					{
//						_playerController.lastHitBy = playerNum;
//						_camShake.ShakeCamera(.05f, false);
//						_audio.PlayOneShot(impact);
//						_playerController.knockbackPower = normalizedHorizontalSpeed;
//						_playerController._currentState = PlayerState.KnockBack;
//					}
//				}
//			}
//		}
//		if(CheckVertical() && _currentState == PlayerState.UpDash)
//		{
//			if(_raycastHit.collider != null)
//			{
//				if(_raycastHit.collider.tag == "Players")
//				{
//					GameObject playerHit = _raycastHit.collider.gameObject;
//					InpCon _playerController = playerHit.GetComponent<InpCon>();
//					if(_playerController._currentState != PlayerState.DashedUp)
//					{
//						_playerController.lastHitBy = playerNum;
//						_camShake.ShakeCamera(.05f, true);
//						audio.PlayOneShot(impact);
//						_playerController._currentState = PlayerState.DashedUp;
//					}
//				}
//			}
//		}
//	}
//
//	private void ResetAnim()
//	{
//		_animator.SetBool("Dodge", false);
//		_animator.SetBool("Stomping", false);
//		_animator.SetBool("AirDodge", false);
//		_animator.SetBool("UpDash", false);
//		_animator.SetBool("Knockback", false);
//		_animator.SetBool("Respawn", false);
//	}
//	/// <summary>
//	/// Handles our player Dodge state.
//	/// </summary>
//	private void Dodge()
//	{
//		if(_timer <= dodge.length)
//		{
//			if(cInput.GetKey("Dodge P" + playerNum.ToString()))
//			   normalizedHorizontalSpeed -= transform.localScale.x * Time.deltaTime;
//			else
//				normalizedHorizontalSpeed -= transform.localScale.x * 3f * Time.deltaTime;
//			_animator.SetBool("Dodge", true);
//			_timer += Time.deltaTime;
//
//			//if we are still dodging we can jump out of it and cancel our dodge state
//			if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && _controller.isGrounded)
//			{
//				_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );
//				_animator.SetBool("Dodge", false);
//				_currentState = PlayerState.Controllable;
//				_timer = 0;
//			}
//			else if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && cInput.GetAxisRaw("Vertical P" + playerNum.ToString()) < 0 && !_controller.isGrounded)
//			{
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_animator.SetBool("Dodge", false);
//				_currentState = PlayerState.Stomp;
//				PlayDashParticles();
//				_timer = 0;
//			}
//			else if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && !_controller.isGrounded)
//			{
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_animator.SetBool("Dodge", false);
//				_currentState = PlayerState.UpDash;
//				PlayDashParticles();
//				_timer = 0;
//			}
//			if(cInput.GetKeyDown("Dodge P" + playerNum.ToString()) && !_controller.isGrounded)
//			{
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_animator.SetBool("Dodge", false);
//				_currentState = PlayerState.AirDodge;
//				PlayDashParticles();
//				normalizedHorizontalSpeed = 3f * transform.localScale.x;
//				_timer = 0;
//			}
//		}
//		else 
//		{
//			_animator.SetBool("Dodge", false);
//			_currentState = PlayerState.Controllable;
//			_timer = 0;
//		}
//
//	}
//
//	/// <summary>
//	/// Handles our AirDodge player state.
//	/// </summary>
//	private void AirDodge()
//	{
//		AirDodged = true;
//		if(_timer <= dodge.length)
//		{
//			if(cInput.GetKey("Dodge P" + playerNum.ToString()))
//			{
//				var adj = transform.localScale.x * 6f * Time.deltaTime;
//				var isGoingRight = transform.localScale.x == 1 ? true : false;
//				normalizedHorizontalSpeed -= adj;
//				if(isGoingRight)
//					_timer = normalizedHorizontalSpeed <= 0 ? dodge.length : _timer;
//				else
//					_timer = normalizedHorizontalSpeed >= 0 ? dodge.length : _timer;
//			}
//			else
//			{
//				var adj = transform.localScale.x * 12f * Time.deltaTime;
//				var isGoingRight = transform.localScale.x == 1 ? true : false;
//				normalizedHorizontalSpeed = normalizedHorizontalSpeed - adj;
//				if(isGoingRight)
//					_timer = normalizedHorizontalSpeed <= 0 ? dodge.length : _timer;
//				else
//					_timer = normalizedHorizontalSpeed >= 0 ? dodge.length : _timer;
//			}
//
//			_velocity.y = 0;
//			_animator.SetBool("AirDodge", true);
//			_timer += Time.deltaTime;
//
//			//we can stomp out of our air dodge, if we do, cancel our airdodge state
//			if(cInput.GetButtonDown("Jump P" + playerNum.ToString()) && cInput.GetAxisRaw("Vertical P" + playerNum.ToString()) < 0 && !_controller.isGrounded)
//			{
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_animator.SetBool("AirDodge", false);
//				_currentState = PlayerState.Stomp;
//				PlayDashParticles();
//				_timer = 0;
//			}
//			else if(cInput.GetKeyDown("Jump P" + playerNum.ToString()) && upDash)
//			{
//				_audio.PlayOneShot(dash[Random.Range(0, dash.Length)]);
//				_currentState = PlayerState.UpDash;
//				PlayDashParticles();
//				_animator.SetBool("AirDodge", false);
//				_timer = 0;
//			}
//		}
//		else
//		{
//			_animator.SetBool("AirDodge", false);
//			_currentState = PlayerState.Controllable;
//			_timer = 0;
//		}
//
//	}
//
//	/// <summary>
//	/// Handles our Death state for our player.
//	/// </summary>
//	private void Die()
//	{
//		_animator.SetBool("Stomping", false);
//		_animator.SetFloat("yVel", 0);
//
//		//Check to see if we still have lives
//		if(GameManager.CheckLives(playerNum) > 0)
//		{
//			if(_timer <= respawnTime) _timer += Time.deltaTime;
//			if(_timer > respawnTime)
//			{
//				_audio.PlayOneShot(death);
//				GameManager.AddKills(lastHitBy, playerNum);
//				GameManager.RemoveLives(playerNum);
//				_gm.RespawnPlayer(playerNum);
//				Destroy(tracker);
//				Destroy(gameObject);
//			}
//		}
//		else if(GameManager.CheckLives(playerNum) == 0)
//		{
//			GameManager.playersAlive--;
//			gameObject.SetActive(false);
//		}
//	}
//
//	/// <summary>
//	/// Handles our stomp state.
//	/// </summary>
//	private void Stomp()
//	{
//		//prevent left and right movement
//		normalizedHorizontalSpeed = 0;
//		//apply a constant downward velocity
//		_velocity.y = stompPower;
//		_animator.SetBool("Stomping", true);
//		//this causes our pause when the player hits the ground, so they cant immediately recover from it
//		if(_controller.isGrounded && _timer <= stompPause)
//		{
//			if(!stompSound)
//			{
//				_stompPart.Play("stompParticles");
//				_audio.PlayOneShot(impact);
//				stompSound = true;
//				_camShake.ShakeCamera(.05f, true);
//			}
//			_timer += Time.deltaTime;
//		} 
//		else if(_controller.isGrounded && _timer > stompPause)
//		{
//			stompSound = false;
//			_animator.SetBool("Stomping", false);
//			_timer = 0;
//			_currentState = PlayerState.Controllable;
//		}
//	}
//
//	/// <summary>
//	/// Calculates movement when player is knocked back.
//	/// </summary>
//	/// <param name="power">Power.</param>
//	public void KnockBack(float power)
//	{
//		if(!hit)
//		{
//			PlayContact();
//			_vrAudio.PlayOneShot(VR[Random.Range(0, VR.Length)]);
//			_animator.SetBool("KnockedBack", true);
//			_velocity.y += knockPower;
//			normalizedHorizontalSpeed = power;
//			hit = true;
//			_timer = 0;
//		}
//		if(hit && _timer <= knockRecovery) _timer += Time.deltaTime;
//		if(hit && _timer > knockRecovery) 
//		{
//			_currentState = PlayerState.Controllable;
//			hit = false;
//			_timer = 0;
//			_animator.SetBool("KnockedBack", false);
//		}
//
//	}
//
//	/// <summary>
//	/// Calculates movement for knockback when player is stomped on.
//	/// </summary>
//	public void StompKnockBack()
//	{
//		if(!hit)
//		{
//			PlayContact();
//			_vrAudio.PlayOneShot(VR[Random.Range(0, VR.Length)]);
//			_animator.SetBool("KnockedBack", true);
//			_velocity.y +=  25;
//			normalizedHorizontalSpeed = 1 * transform.localScale.x;
//			hit = true;
//			_timer = 0;
//		}
//		if(hit && _timer <= knockRecovery)
//		{
//			_timer += Time.deltaTime;
//			normalizedHorizontalSpeed -= Time.deltaTime * transform.localScale.x;
//		}
//		if(hit && _timer > knockRecovery) 
//		{
//			_currentState = PlayerState.Controllable;
//			hit = false;
//			_timer = 0;
//			_animator.SetBool("KnockedBack", false);
//		}
//		
//	}
//
//	void OnTriggerStay2D ( Collider2D col )
//	{
//		if(col.tag == "DeathArea")
//		{
//			_animator.SetBool("Dodge", false);
//			_animator.SetBool ("AirDodge", false);
//			_animator.SetBool("Stomping", false);
//			_currentState = PlayerState.Dead;
//			Debug.Log ("Dead "+ playerNum);
//		}
//	}
//
//	/// <summary>
//	/// Checks the sides.
//	/// </summary>
//	/// <returns><c>true</c>, if there is an object to the side we are travelling, <c>false</c> otherwise.</returns>
//	bool CheckSides()
//	{
//		setRaycastOrigins();
//		var isGoingRight = transform.localScale.x > 0;
//		var rayDirection = isGoingRight ? Vector2.right : -Vector2.right;
//		var initialRayOrigin = isGoingRight ? _raycastOrigins.bottomRight : _raycastOrigins.bottomLeft;
//		var rayDistance = .1f;
//		for(int i = 0; i < totalHorizontalRays; i++)
//		{
//			var ray = new Vector2(initialRayOrigin.x, initialRayOrigin.y + i * _horizontalDistanceBetweenRays);
//			_raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
//			if(_raycastHit)
//				return true;
//		}
//		return false;
//	}
//
//	/// <summary>
//	/// Checks the vertical.
//	/// </summary>
//	/// <returns><c>true</c>, if we have an object in the direction we are travelling, <c>false</c> otherwise.</returns>
//	bool CheckVertical()
//	{
//		setRaycastOrigins();
//		var isDashingUp = _currentState == PlayerState.UpDash;
//		var rayDirection = isDashingUp ? Vector2.up: -Vector2.up;
//		var initialRayOrigin = isDashingUp ? _raycastOrigins.topLeft : _raycastOrigins.bottomLeft;
//		var rayDistance = 1f;
//		for(int i = 0; i < totalVerticalRays; i++)
//		{
//			var ray = new Vector2(initialRayOrigin.x + i * _verticalDistanceBetweenRays, initialRayOrigin.y);
//			_raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
//			if(_raycastHit)
//				return true;
//		}
//		return false;
//	}
//
//	/// <summary>
//	/// Recalculates the distance between rays if we change the number of rays in code we must call this.
//	/// </summary>
//	public void recalculateDistanceBetweenRays()
//	{
//		// figure out the distance between our rays in both directions
//		// horizontal
//		var colliderUseableHeight = _box.size.y * Mathf.Abs( transform.localScale.y ) - ( 2f * _skinWidth );
//		_verticalDistanceBetweenRays = colliderUseableHeight / ( totalHorizontalRays - 1 );
//		
//		// vertical
//		var colliderUseableWidth = _box.size.x * Mathf.Abs( transform.localScale.x ) - ( 2f * _skinWidth );
//		_horizontalDistanceBetweenRays = colliderUseableWidth / ( totalVerticalRays - 1 );
//	}
//
//
//	/// <summary>
//	/// Sets the raycast origins, must be called anytime you cast rays.
//	/// </summary>
//	private void setRaycastOrigins()
//	{
//		var	scaledColliderSize = new Vector2 (_box.size.x * Mathf.Abs (transform.localScale.x), _box.size.y * Mathf.Abs (transform.localScale.y)) / 2;
//		var scaledCenter = new Vector2( _box.center.x * transform.localScale.x, _box.center.y * transform.localScale.y );
//		
//		_raycastOrigins.topRight = transform.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
//		_raycastOrigins.topRight.x -= _skinWidth;
//		_raycastOrigins.topRight.y -= _skinWidth;
//		
//		_raycastOrigins.topLeft = transform.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
//		_raycastOrigins.topLeft.x += _skinWidth;
//		_raycastOrigins.topLeft.y -= _skinWidth;
//		
//		_raycastOrigins.bottomRight = transform.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
//		_raycastOrigins.bottomRight.x -= _skinWidth;
//		_raycastOrigins.bottomRight.y += _skinWidth;
//		
//		_raycastOrigins.bottomLeft = transform.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
//		_raycastOrigins.bottomLeft.x += _skinWidth;
//		_raycastOrigins.bottomLeft.y += _skinWidth;
//	}
//}
