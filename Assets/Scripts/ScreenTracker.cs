﻿using UnityEngine;
using System.Collections;

public class ScreenTracker : MonoBehaviour {

	public GameObject goToTrack;
	
	void Update () {
		Vector3 v3Screen = Camera.main.WorldToViewportPoint(goToTrack.transform.position);
		if (v3Screen.x > -0.01f && v3Screen.x < 1.01f && v3Screen.y > -0.01f && v3Screen.y < 1.01f)
			renderer.enabled = false;
		else
		{
			renderer.enabled = true;
			v3Screen.x = Mathf.Clamp (v3Screen.x, 0.03f, 0.97f);
			v3Screen.y = Mathf.Clamp (v3Screen.y, 0.03f, 0.97f);
			transform.position = Camera.main.ViewportToWorldPoint (v3Screen);
			// LookAt 2D
			Vector3 target = goToTrack.transform.position;
			// get the angle
			Vector3 norTar = (target - transform.position).normalized;
			float angle = Mathf.Atan2(norTar.y,norTar.x)*Mathf.Rad2Deg;
			// rotate to angle
			Quaternion rotation = new Quaternion ();
			rotation.eulerAngles = new Vector3(0,0,angle-90);
			transform.rotation = rotation;
		}
		
	}

	public void SetTracker(GameObject go)
	{
		goToTrack = go;
	}
}
