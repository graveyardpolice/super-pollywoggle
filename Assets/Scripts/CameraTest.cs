﻿using UnityEngine;
using System.Collections;

public class CameraTest : MonoBehaviour {

	float pixelToUnit = 10f;

	// Use this for initialization
	void Start () 
	{
		camera.aspect = 16f/9f;
		camera.orthographicSize = (Screen.height / 2 / pixelToUnit);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
