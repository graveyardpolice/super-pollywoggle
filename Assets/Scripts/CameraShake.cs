﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	Vector3 originalCameraPosition;
	Camera mainCamera;
	float shakeAmount;
	bool shakeVert;

	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;
		originalCameraPosition = mainCamera.transform.position;
	}

	/// <summary>
	/// Shakes the camera.
	/// </summary>
	/// <param name="strength">Strength</param>
	public void ShakeCamera(float strength, bool shakeVertical)
	{
		if(!GameManager.isScreenShaking)
		{
			GameManager.isScreenShaking = true;
			shakeAmount = strength;
			shakeVert = shakeVertical;
			InvokeRepeating("StartShake", 0f, .005f);
			Invoke("StopShake", .1f);
		}
	}


	private void StartShake()
	{
		if(shakeAmount > 0)
		{
			float quakeAmount = Random.value * shakeAmount * 2 - shakeAmount;
			Vector3 cPos = mainCamera.transform.position;
			cPos.y += shakeVert ? quakeAmount : 0;
			cPos.x += shakeVert ? 0 : quakeAmount;
			mainCamera.transform.position = cPos;
		}
	}

	private void StopShake()
	{
		if(GameManager.isScreenShaking)GameManager.isScreenShaking = false;
		CancelInvoke("StartShake");
		mainCamera.transform.position = originalCameraPosition;
	}
}
