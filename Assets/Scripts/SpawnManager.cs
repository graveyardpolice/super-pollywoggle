//using UnityEngine;
//using System.Collections;
//
//public class SpawnManager : MonoBehaviour {
//	
//	private PlayerChar _playerControl; //local variable to temporarily cache our InputManagers to assign controls before spawn
//	private GameObject[] spawns; //array to store our random spawn points
//
//	// Use this for initialization
//	void Start () 
//	{
//		//cache our spawn points
//		spawns = GameObject.FindGameObjectsWithTag("Spawn");
//
//		//reset our players alive status
//		GameManager.playersAlive = 0;
//
//		//grab information about players joining match from our manager
//		//loop through and assign our controler inputs to each players character
//		GameObject[] spawning = new GameObject[GameManager.playersToSpawn.Length];
//		Debug.Log (spawning.Length);
//		for(int i = 0; i < spawning.Length; i++)
//		{
//			if(GameManager.playersToSpawn[i] != null)
//			{
//				spawning[i] = (GameObject)Instantiate(GameManager.playersToSpawn[i]);
//				_playerControl = spawning[i].GetComponent<PlayerChar>();
//				_playerControl.playNum = GameManager.playerControllers[i];
//				//spawn our players once they are set up
//				SpawnPlayer(spawning[i]);
//				//add one to our playersAlive for each player we process
//				GameManager.playersAlive++;
//			}
//		}
//		//TODO: move this to game setup menu, its temporary for now until the game setup gets implemented
//		GameManager._gameMode = GameManager.GameMode.FFADeathMatch;
//		//TODO:move this to the game setup as well
//		GameManager.numOfLives = 10;
//		//TODO: move to game setup menu as well
//		GameManager.SetUpGame(spawning.Length);
//	}
//	#region Debug testing code
//	//TODO: TEMPORARY DEBUG CODE
//	#endregion
//	
//	private void SpawnPlayer(GameObject go)
//	{
//		int spawn = Random.Range(0, spawns.Length);
//		PlayerChar player = go.GetComponent<PlayerChar>();
//		go.transform.position = spawns[spawn].transform.position;
//	}
//	//public method that can be called by our players when they are able to be spawned
//	//TODO: Make it check that there isnt a player at this location already.
//	public void RespawnPlayer(int playerNum)
//	{
//		var i = playerNum - 1;
//		GameObject go = (GameObject)Instantiate(GameManager.playersToSpawn[i]);
//		int spawn = Random.Range(0, spawns.Length);
//		InpCon player = go.GetComponent<InpCon>();
//		go.transform.position = spawns[spawn].transform.position;
//		player.playerNum = playerNum;
//
//	}
//
//
//}
