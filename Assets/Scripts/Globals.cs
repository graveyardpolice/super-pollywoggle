﻿using UnityEngine;
using System.Collections;

public struct CharacterRaycastOrigins
{
	public Vector3 topRight;
	public Vector3 topLeft;
	public Vector3 bottomRight;
	public Vector3 bottomLeft;
}

public class AirStates
{
	public bool airDash;
	public bool upDash;
	
	public void Reset()
	{
		airDash = upDash = false;
	}
}


public class Globals : MonoBehaviour {
	#region Internal Types

	#endregion
	private static Globals m_instance;


	public static Globals Instance
	{
		get
		{
			if(m_instance == null)
				m_instance = new Globals();
			return m_instance;
		}
	}
	
	public float runSpeed = 20f; //Our speed we move at
	public float groundDamping = 20f; //How fast we change direction
	public float gravity = -50f; //gravity applied to our player every frame
	public float jumpHeight = 8f; //the strength of our jump
	public float upDashPower = 60f; //strength of our dash
	public float stompPower = -75f; //strength of our stomp
	public float dashTimer = .35f; //length of our dash
	public float stompPause = .5f; //length we pause after hitting the ground on a stomp
	public float bounceUp = 10f; //amount we lift up on a knock back
	public float recoveryDelay = .5f; //amount of time we are uncontrollable while knocked back
	public float knockedUpPower = 10f; //amount of lift on knocked up states
	public float dodge = .66f; //dodge animation used for length
	public int totalVerticalRays = 4;
	public int totalHorizontalRays = 8;
	public float horRayDist;
	public float vertRayDist;

	public bool CheckSides(Transform trans, CharacterRaycastOrigins origins,
	                        BoxCollider2D box, float skinWidth, LayerMask playerMask, out RaycastHit2D hit)
	{
		Globals.Instance.setRaycastOrigins(origins, box, skinWidth, trans);
		var isGoingRight = trans.localScale.x > 0;
		var rayDirection = isGoingRight ? Vector2.right : -Vector2.right;
		var initialRayOrigin = isGoingRight ? origins.bottomRight : origins.bottomLeft;
		var rayDistance = .1f;
		for(int i = 0; i < totalHorizontalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x, initialRayOrigin.y + i * horRayDist);
			var rayhit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(rayhit)
			{
				hit = rayhit;
				return true;
			}
		}
		//we need to return an empty hit info if we didnt hit anything
		hit = new RaycastHit2D();
		return false;
	}

	public bool CheckVertical(Transform trans, CharacterRaycastOrigins origins,
	                           BoxCollider2D box, float skinWidth, LayerMask playerMask, FSM2DState state, out RaycastHit2D hit)
	{
		Globals.Instance.setRaycastOrigins(origins, box, skinWidth, trans);
		var isDashingUp = state.state == "UpDash";
		var rayDirection = isDashingUp ? Vector2.up: -Vector2.up;
		var initialRayOrigin = isDashingUp ? origins.topLeft : origins.bottomLeft;
		var rayDistance = 1f;
		for(int i = 0; i < totalVerticalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x + i * vertRayDist, initialRayOrigin.y);
			var rayhit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(rayhit)
			{
				hit = rayhit;
				return true;
			}
		}
		//return empty hit info if we hit nothing
		hit = new RaycastHit2D();
		return false;
	}

	public void setRaycastOrigins(CharacterRaycastOrigins origins, BoxCollider2D box, float skinWidth, Transform trans)
	{
		var	scaledColliderSize = new Vector2 (box.size.x * Mathf.Abs (trans.localScale.x), box.size.y * Mathf.Abs (trans.localScale.y)) / 2;
		var scaledCenter = new Vector2(box.center.x * trans.localScale.x, box.center.y * trans.localScale.y );
		
		origins.topRight = trans.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
		origins.topRight.x -= skinWidth;
		origins.topRight.y -= skinWidth;
		
		origins.topLeft = trans.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y + scaledColliderSize.y );
		origins.topLeft.x += skinWidth;
		origins.topLeft.y -= skinWidth;
		
		origins.bottomRight = trans.position + new Vector3( scaledCenter.x + scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
		origins.bottomRight.x -= skinWidth;
		origins.bottomRight.y += skinWidth;
		
		origins.bottomLeft = trans.position + new Vector3( scaledCenter.x - scaledColliderSize.x, scaledCenter.y -scaledColliderSize.y );
		origins.bottomLeft.x += skinWidth;
		origins.bottomLeft.y += skinWidth;
	}

	public void recalculateRayDist(BoxCollider2D box, float skinWidth, Transform trans)
	{
		var colliderUseableHeight = box.size.y * Mathf.Abs( trans.localScale.y ) - ( 2f * skinWidth );
		vertRayDist = colliderUseableHeight / ( totalHorizontalRays - 1 );
		
		// vertical
		var colliderUseableWidth = box.size.x * Mathf.Abs( trans.localScale.x ) - ( 2f * skinWidth );
		horRayDist = colliderUseableWidth / ( Instance.totalVerticalRays - 1 );
	}
}
