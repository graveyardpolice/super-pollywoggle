﻿using UnityEngine;
using System.Collections;

public class ScreenshotSatTemp : MonoBehaviour {

	SpriteRenderer sprite;
	Color alpha;
	Transform myTrans;
	public float fade, moveSpeed;
	bool adjust;
	// Use this for initialization
	void Start () 
	{
		myTrans = GetComponent<Transform>();
		sprite = GetComponent<SpriteRenderer>();
		alpha = sprite.color;
		alpha.a = fade;
		sprite.color = alpha;
	}
	
	// Update is called once per frame
	void Update () 
	{

		Vector3 adjTrans = myTrans.position;
		adjTrans.x += Time.deltaTime * moveSpeed;
		myTrans.position = adjTrans;
	
	}

	void OnBecameInvisible()
	{
		if(adjust)
		{
			adjust = false;
			myTrans.position = new Vector3(-25, myTrans.position.y, myTrans.position.z);
		}
	}

	void OnBecameVisible()
	{
		adjust = true;
	}
}
