﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {

	public GUISkin style;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUI.skin = style;
		for(int i = 0; i < GameManager.playerLives.Length; i++)
		{
			if(GameManager.playerLives[i] >= 0 && GameManager.playersToSpawn[i] != null)
			{
				if(i <= 1)
				{
					float padding = i == 0 ? 0 : Screen.width - 100;
					GUI.Box (new Rect(padding, 0, 100, 25), "P" + (i + 1) + " - " + GameManager.playerLives[i]);
				}
				if(i > 1)
				{
					float paddingX = i == 2 ? 0 : Screen.width - 100;
					float paddingY = Screen.height - 25;
					GUI.Box(new Rect(paddingX, paddingY, 100, 25), "P" + (i + 1) + " - " + GameManager.playerLives[i]);
				}
			}
		}
	}
}
