﻿using UnityEngine;
using System.Collections;

public class EndMatch : MonoBehaviour {

	private float padding = 10;
	private float _screenWidth, _screenHeight;
	private bool drawEnd;
	private int highNum;
	private int[] nemesis = new int[4];
	private int[] totalKills = new int[4];
	private int first, second, third, fourth;
	public GUISkin skin;
	public bool statCalc;

	void Awake()
	{
		_screenWidth = Screen.width;
		_screenHeight = Screen.height;
	}
	// Use this for initialization
	void Start () 
	{
		statCalc = false;
		drawEnd = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(GameManager.playersAlive <=1)
		{
			if(!statCalc)
			{
				CalculateStats();
				CalculateWinners();
			}
			statCalc = true;
			drawEnd = true;
		} 
	}

	void OnGUI()
	{
		if(drawEnd)
		{
			GUI.skin = skin;
			//create 4 boxes for each player to select a character from
			//TODO: add textures and allow players to pick which character they can play as instead of auto assigning each player to a specific one
			//player 1
			if(GameManager.playersToSpawn[0] != null)
			{
			GUI.Box(new Rect(0 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 1");
				GUI.Label(new Rect(0 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "ArchEnemy: " + "Player "  +nemesis[0] + "\n \n Total Kills: " + totalKills[0]
				          + "\n\n Press ESC/B(360) for Menu \n\n Enter / Start(360) to Replay");
			}
			//player 2
			if(GameManager.playersToSpawn[1] != null)
			{
			GUI.Box(new Rect(1 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 2");
				GUI.Label(new Rect(1 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "ArchEnemy: " + "Player "  +nemesis[1] + "\n \n Total Kills: " + totalKills[1]
				          + "\n\n Press ESC/B(360) for Menu \n\n Enter / Start(360) to Replay");
			}
			//player 3
			if(GameManager.playersToSpawn[2] != null)
			{
			GUI.Box(new Rect(2 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 3");
				GUI.Label(new Rect(2 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "ArchEnemy: " + "Player "  +nemesis[2] + "\n \n Total Kills: " + totalKills[2]
				          + "\n\n Press ESC/B(360) for Menu \n\n Enter / Start(360) to Replay");
			}
			//player 4
			if(GameManager.playersToSpawn[3] != null)
			{
			GUI.Box(new Rect(3 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "Player 4");
			GUI.Label(new Rect(3 * (_screenWidth / 4) + padding, padding, _screenWidth/4 - (padding * 2), _screenHeight - (padding * 2)), "ArchEnemy: " + "Player "  +nemesis[3] + "\n \n Total Kills: " + totalKills[3]
				          + "\n\n Press ESC/B(360) for Menu \n\n Enter / Start(360) to Replay");
			}
		}
	}

	private void CalculateWinners()
	{
		for(int i = 0; i < totalKills.Length; i++)
		{
			switch(i + 1)
			{
			case 1:
				foreach(int num in GameManager.killTracker.player1)
					totalKills[i] += num;
				break;
			case 2:
				foreach(int num in GameManager.killTracker.player2)
					totalKills[i] += num;
				break;
			case 3:
				foreach(int num in GameManager.killTracker.player3)
					totalKills[i] += num;
				break;
			case 4:
				foreach(int num in GameManager.killTracker.player4)
					totalKills[i] += num;
				break;
			}
		}
	}
	private void CalculateStats()
	{
		for(int i = 0; i < GameManager.playersToSpawn.Length; i++)
		{
			if(GameManager.playersToSpawn[i] != null)
			{
				highNum = 0;
				switch(i + 1)
				{
				case 1:
					if(GameManager.killTracker.player2[i] > highNum)
					{
						highNum = GameManager.killTracker.player2[i];
						nemesis[i] = 2;
					}
					if(GameManager.killTracker.player3[i] > highNum)
					{
						highNum = GameManager.killTracker.player3[i];
						nemesis[i] = 3;
					}
					if(GameManager.killTracker.player4[i] > highNum)
					{
						highNum = GameManager.killTracker.player4[i];
						nemesis[i] = 4;
					}
					break;
					
				case 2:
					if(GameManager.killTracker.player1[i] > highNum)
					{
						highNum = GameManager.killTracker.player1[i];
						nemesis[i] = 1;
					}
					if(GameManager.killTracker.player3[i] > highNum)
					{
						highNum = GameManager.killTracker.player3[i];
						nemesis[i] = 3;
					}
					if(GameManager.killTracker.player4[i] > highNum)
					{
						highNum = GameManager.killTracker.player4[i];
						nemesis[i] = 4;
					}
					break;
					
				case 3:
					if(GameManager.killTracker.player1[i] > highNum)
					{
						highNum = GameManager.killTracker.player1[i];
						nemesis[i] = 1;
					}
					if(GameManager.killTracker.player2[i] > highNum)
					{
						highNum = GameManager.killTracker.player2[i];
						nemesis[i] = 2;
					}
					if(GameManager.killTracker.player4[i] > highNum)
					{
						highNum = GameManager.killTracker.player4[i];
						nemesis[i] = 4;
					}
					break;
					
				case 4:
					if(GameManager.killTracker.player1[i] > highNum)
					{
						highNum = GameManager.killTracker.player1[i];
						nemesis[i] = 1;
					}
					if(GameManager.killTracker.player2[i] > highNum)
					{
						highNum = GameManager.killTracker.player2[i];
						nemesis[i] = 2;
					}
					if(GameManager.killTracker.player3[i] > highNum)
					{
						highNum = GameManager.killTracker.player3[i];
						nemesis[i] = 3;
					}
					break;
				}
			}
		}
	}
}
