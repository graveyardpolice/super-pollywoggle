﻿using UnityEngine;
using System.Collections;

public class AdjustCamera : MonoBehaviour {

	public float pixelToUnit;
	public float zoom;
	
	const float targetAspect = 16.0f/9.0f;
	
	// Use this for initialization
	void Start () 
		
	{
		float windowAspect = (float)Screen.width / (float)Screen.height;

		float scaleheight = windowAspect / targetAspect;

		Camera camera = GetComponent<Camera>();

		if(scaleheight < 1.0f)
		{
			Rect rect = camera.rect;

			rect.width = 1.0f;
			rect.height = scaleheight;
			rect.x = 0;
			rect.y = (1.0f - scaleheight) / 2;

			camera.rect = rect;
		}
		else
		{
			float scaleWidth = 1.0f / scaleheight;

			Rect rect = camera.rect;

			rect.width = scaleWidth;
			rect.height = 1.0f;
			rect.x = (1.0f - scaleWidth) / 2;
			rect.y = 0;

			camera.rect = rect;
		}
		camera.orthographicSize = (camera.pixelHeight / 2 / pixelToUnit) * zoom;
		Debug.Log("Camera is " + camera.pixelHeight + " Pixels high");
	}
	
	// Update is called once per frame
	void Update () 
	{
		/**if(Input.GetKeyDown(KeyCode.J))
		{
			zoom = 1f;
			camera.orthographicSize = (camera.pixelHeight / 2 / pixelToUnit) * zoom;
		}
		
		if(Input.GetKeyDown(KeyCode.K))
		{
			zoom = .75f;
			camera.orthographicSize = (camera.pixelHeight / 2 / pixelToUnit) * zoom;
		}
		
		if(Input.GetKeyDown(KeyCode.L))
		{
			zoom = .5f;
			camera.orthographicSize = (camera.pixelHeight / 2 / pixelToUnit) * zoom;
		}**/

		
		
	}
}