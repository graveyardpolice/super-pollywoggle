﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using InControl;

public class PlayerChar : FSM2D {

#region Public Variables
	public int playNum;	//Zero-based player number.
	public LayerMask playerMask; //Players we can collide with
#endregion

#region Private Variable
	private float axisRaw; //axis reading between -1 and 1;
	private float normHorSpeed; //variable that movement calculations are based off of
	private Transform m_transform; //Cache for our transform
	private float m_timer;	//cache for our timer
	private Vector3 m_velocity; //cache for our velocity from our controller
	private CharacterController2D m_control; //cache for our character controller 2D
	private AirStates m_airStates; //Cache for our air states
	private Animator m_animator; //cache for our animator
	private RaycastHit2D m_rayhit; //holds raycast hit info
	private BoxCollider2D m_box; //cache for our box collider
	private CharacterRaycastOrigins m_origins; //cache for our ray origins
	private float m_knockPower; //amount we will be knocked back
	private bool firstPass; //if this is our first pass through an update, this is a workaround for when we modify m_velocity
	private InputDevice inputDevice;
#endregion

#region Properties
	private Transform trans
	{
		get
		{
			if(m_transform == null)
				m_transform = GetComponent<Transform>();
			return m_transform;
		}
	}
	private CharacterController2D control
	{
		get
		{
			if(m_control == null)
				m_control = GetComponent<CharacterController2D>();
			return m_control;
		}
	}
	private Animator anim
	{
		get
		{
			if(m_animator == null)
				m_animator = GetComponent<Animator>();
			return m_animator;
		}
	}
	private float skinWidth
	{
		get
		{
			return control.skinWidth;
		}
	}
	private BoxCollider2D box
	{
		get
		{
			if(m_box == null)
				m_box = GetComponent<BoxCollider2D>();
			return m_box;
		}
	}
	private CharacterRaycastOrigins origins
	{
		get
		{
			return m_origins;
		}
	}
	private int vertRays
	{
		get
		{
			return Globals.Instance.totalVerticalRays;
		}
	}
	private int horRays
	{
		get
		{
			return Globals.Instance.totalHorizontalRays;
		}
	}

	private AirStates airStates
	{
		get
		{
			if(m_airStates == null)
				m_airStates = new AirStates();
			return m_airStates;
		}
	}
	public string currentState
	{
		get
		{
			return curState.state;
		}
		set
		{
			SwitchState(value);
		}
	}
	public float knockPower
	{
		get{return m_knockPower;}
		set{m_knockPower = value;}
	}
	private float runSpeed
	{
		get
		{
			return Globals.Instance.runSpeed;
		}
	}
	private float groundDamping
	{
		get
		{
			return Globals.Instance.groundDamping;
		}
	}
	private float gravity
	{
		get
		{
			return Globals.Instance.gravity;
		}
	}
	private float jumpHeight
	{
		get
		{
			return Globals.Instance.jumpHeight;
		}
	}
	private float upDashPower
	{
		get
		{
			return Globals.Instance.upDashPower;
		}
	}
	private float dashTimer
	{
		get
		{
			return Globals.Instance.dashTimer;
		}
	}
	private float dashLength
	{
		get
		{
			return Globals.Instance.dodge;
		}
	}
	private float stompPower
	{
		get
		{
			return Globals.Instance.stompPower;
		}
	}
	private float stompPause
	{
		get
		{
			return Globals.Instance.stompPause;
		}
	}
	private float recoveryDelay
	{
		get
		{
			return Globals.Instance.recoveryDelay;
		}
	}
	private float knockedUpPower
	{
		get
		{
			return Globals.Instance.knockedUpPower;
		}
	}
#endregion
	
	void Awake()
	{
		SetUpState("Controllable");
		SetUpState("Run");
		SetUpState("UpDash");
		SetUpState("AirDash");
		SetUpState("Stomp");
		SetUpState("Dash");
		SetUpState("Knockedback");
		SetUpState("KnockedUp");
		SwitchState("Controllable");
	}
	
	void Start()
	{

	}


#region State-Independent Update

	protected override void PreUpdate()
	{
		anim.SetBool("Grounded", control.isGrounded);
		m_velocity = control.velocity;
		anim.SetFloat("yVel", m_velocity.y);
		if(control.isGrounded)
			airStates.Reset();
		inputDevice = InputManager.Devices.Count > playNum ? InputManager.Devices[playNum] : null;
	}
	protected override void PostUpdate()
	{
		//TODO: edit these two lines, they are temporary to close the application and reload the level easily
//		if(CheckStart())
//		{
//			GameManager.ResetGame();
//			Application.LoadLevel(Application.loadedLevel);
//		} 
		if(Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.LoadLevel("Menu");
			GameManager.playersToSpawn = new GameObject[4];
		}

		//set our speed in our animator to be equal to our normalized speed
		anim.SetFloat("Speed", Mathf.Abs(normHorSpeed));

		//apply adjustments to the normalized speed if we have an item effect on us
//		NormHorAdjust();

		// apply horizontal speed smoothing it
		m_velocity.x = Mathf.Lerp( m_velocity.x, normHorSpeed * runSpeed, Time.deltaTime * groundDamping );

		// apply gravity before moving, if we are in a state with no gravity, do nothing;
		m_velocity.y += curState.state != "AirDash" && curState.state != "Respawn" ? gravity * Time.deltaTime : 0;

		control.move(m_velocity * Time.deltaTime);
	}
#endregion

#region State-Independent Collisions
	protected override void ConstTriggerEnter2D(Collider2D col)
	{

	}
#endregion

#region Control State
	private void Controllable_Update()
	{
		if(inputDevice != null)axisRaw = inputDevice.LeftStickX.Value;

		normHorSpeed += axisRaw * 2.5f * Time.deltaTime;
		
		//Flip our sprite based on which way we are travelling
		if(axisRaw < 0 && trans.localScale.x > 0)
			trans.localScale = new Vector3( -trans.localScale.x, trans.localScale.y, trans.localScale.z);
		else if(axisRaw > 0 && trans.localScale.x < 0) 
			trans.localScale = new Vector3( -trans.localScale.x, trans.localScale.y, trans.localScale.z);

		if(axisRaw == 0)
		{
			if(trans.localScale.x == 1)
			{
				normHorSpeed = normHorSpeed > 0 ? normHorSpeed - (2.5f * Time.deltaTime) : 0;
			}
			if(trans.localScale.x == -1)
			{
				normHorSpeed = normHorSpeed < 0 ? normHorSpeed + (2.5f * Time.deltaTime) : 0;
			}
		}

		//If our horizontal speed is greater than 1, force it to be 1 or -1 based on our direction
		normHorSpeed = Mathf.Abs(normHorSpeed) > 1 ? trans.localScale.x : normHorSpeed;

		//Jump if we received input and are grounded
		if(inputDevice != null)
		{
			if(inputDevice.Action1.WasPressed && inputDevice.LeftStickY.Value < -0.5f && !control.isGrounded)
			{
				SwitchState("Stomp");
			}
			else if(inputDevice.Action1.WasPressed && control.isGrounded)
			{
				m_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );
			}
			else if(inputDevice.Action1.WasReleased && !control.isGrounded && !airStates.upDash && m_velocity.y > 0) 
				m_velocity.y = 0;
			else if(inputDevice.Action1.WasPressed && !control.isGrounded && !airStates.upDash)
			{
				airStates.upDash = true;
				SwitchState("UpDash");
			}
			if(inputDevice.Action3.WasPressed)
			{
				if(control.isGrounded)
					SwitchState("Dash");
				if(!control.isGrounded && !airStates.airDash)
					SwitchState("AirDash");
			}
		}
	}
#endregion

#region UpDash state
	private void UpDash_Enter()
	{
		m_timer = 0;
		anim.SetBool("UpDash", true);
		m_velocity.y = upDashPower;
	}

	private void UpDash_Update()
	{
		m_timer += Time.deltaTime;
		if(m_timer >= dashTimer)
		{
			SwitchState("Controllable");
			return;
		}
		Collisions();
		m_velocity.y -= upDashPower * Time.deltaTime * 2;
		normHorSpeed = 0;
		if(inputDevice.Action3.WasPressed && !airStates.airDash)
		{
			SwitchState("AirDash");
		}

	}

	private void UpDash_Exit()
	{
		anim.SetBool("UpDash", false);
	}
#endregion

#region AirDash State
	private void AirDash_Enter()
	{
		m_timer = 0;
		airStates.airDash = true;
		anim.SetBool("AirDodge", true);
		normHorSpeed = 2.75f * trans.localScale.x;
		m_velocity.y = 0;
	}

	private void AirDash_Update()
	{
		m_timer += Time.deltaTime;
		if(m_timer >= dashLength)
		{
			SwitchState("Controllable");
			return;
		}
		Collisions();
		if(inputDevice.Action3.IsPressed)
		{
			var adj = trans.localScale.x * 6f * Time.deltaTime;
			var isGoingRight = trans.localScale.x == 1 ? true : false;
			normHorSpeed -= adj;
			if(isGoingRight)
				m_timer = normHorSpeed <= 0 ? dashLength : m_timer;
			else
				m_timer = normHorSpeed >= 0 ? dashLength : m_timer;
		}
		else
		{
			var adj = trans.localScale.x * 12f * Time.deltaTime;
			var isGoingRight = trans.localScale.x == 1 ? true : false;
			normHorSpeed -= adj;
			if(isGoingRight)
				m_timer = normHorSpeed <= 0 ? dashLength : m_timer;
			else
				m_timer = normHorSpeed >= 0 ? dashLength : m_timer;
		}
		if(inputDevice.Action1.WasPressed && inputDevice.LeftStickY.Value < 0)
		{
			SwitchState("Stomp");
			return; 
		}
		else if(inputDevice.Action1.WasPressed && !airStates.upDash && !control.isGrounded)
		{
			SwitchState ("UpDash");
			return;
		}
	}

	private void AirDash_Exit()
	{
		anim.SetBool("AirDodge", false);
	}
#endregion

#region Stomp State
	private void Stomp_Enter()
	{
		anim.SetBool("Stomping", true);
		m_timer = 0;
	}

	private void Stomp_Update()
	{
		normHorSpeed = 0;
		m_velocity.y = stompPower;
		Collisions();
		if(control.isGrounded)
		{
			m_timer += Time.deltaTime;
		}
		if(m_timer >= stompPause)
			SwitchState("Controllable");
	}

	private void Stomp_Exit()
	{
		anim.SetBool("Stomping", false);
	}
#endregion

#region Dash State
	private void Dash_Enter()
	{
		m_timer = 0;
		anim.SetBool("Dodge", true);
		normHorSpeed = 2 * trans.localScale.x;
	}

	private void Dash_Update()
	{
		m_timer += Time.deltaTime;
		if(m_timer >= dashLength)
		{
			SwitchState("Controllable");
			return;
		}
		Collisions();
		if(inputDevice.Action3.IsPressed)
			normHorSpeed -= trans.localScale.x * Time.deltaTime;
		else
			normHorSpeed -= trans.localScale.x * 3f * Time.deltaTime;
		if(inputDevice.Action1.WasPressed && control.isGrounded)
		{
			m_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );
			SwitchState("Controllable");
			return;
		}
		else if(inputDevice.Action1.WasPressed && inputDevice.LeftStickY < 0 && !control.isGrounded)
		{
			SwitchState("Stomp");
			return;
		}
		else if(inputDevice.Action1.WasPressed && !control.isGrounded && !airStates.upDash)
		{
			SwitchState("UpDash");
			return;
		}
		else if(inputDevice.Action3.WasPressed && !control.isGrounded && !airStates.airDash)
		{
			SwitchState("AirDash");
			return;
		}
	}

	private void Dash_Exit()
	{
		anim.SetBool("Dodge", false);
	}
#endregion

#region Knockedback State
	private void Knockedback_Enter()
	{
		normHorSpeed = knockPower;
		m_timer = 0;
		firstPass = true;
		anim.SetBool("KnockedBack", true);
	}

	private void Knockedback_Update()
	{
		if(firstPass)
		{
			m_velocity.y += knockPower;
			firstPass = false;
		}
		m_timer += Time.deltaTime;
		if(m_timer >= recoveryDelay)
		{
			SwitchState("Controllable");
			return;
		}
	}

	private void Knockedback_Exit()
	{
		anim.SetBool("KnockedBack", false);
	}
#endregion

#region KnockedUp State
	private void KnockedUp_Enter()
	{
		firstPass = true;
		anim.SetBool("KnockedBack", true);
		normHorSpeed = trans.localScale.x;
		m_timer = 0;
	}

	private void KnockedUp_Update()
	{
		if(firstPass)
		{
			m_velocity.y += knockedUpPower;
			firstPass = false;
		}
		m_timer += Time.deltaTime;
		if(m_timer >= recoveryDelay)
		{
			SwitchState("Controllable");
			return;
		}
		normHorSpeed -= Time.deltaTime * trans.localScale.x;
	}

	private void KnockedUp_Exit()
	{
		anim.SetBool("KnockedBack", false);
	}
#endregion

	public void PlayFootstep(){}

#region Collision Methods
	private void Collisions()
	{
		if(CheckVertical() && currentState == "Stomp")
		{
			if(m_rayhit.collider != null)
			{
				if(m_rayhit.collider.tag == "Players")
				{
					GameObject playerHit = m_rayhit.collider.gameObject;
					PlayerChar _playerController = playerHit.GetComponent<PlayerChar>();
					if(_playerController.currentState != "KnockedUp")
					{
						_playerController.currentState = "KnockedUp";
					}
				}
			}
		}
		else if(CheckVertical() && currentState == "UpDash")
		{
			if(m_rayhit.collider != null)
			{
				if(m_rayhit.collider.tag == "Players")
				{
					GameObject playerHit = m_rayhit.collider.gameObject;
					PlayerChar _playerController = playerHit.GetComponent<PlayerChar>();
					if(_playerController.currentState != "KnockedUp")
					{
						_playerController.currentState = "KnockedUp";
					}
				}
			}
		}
		else if(CheckSides() && (currentState == "AirDash" || currentState == "Dash"))
		{
			if(m_rayhit.collider != null)
			{
				if(m_rayhit.collider.tag == "Players")
				{
					GameObject playerHit = m_rayhit.collider.gameObject;
					PlayerChar _playerController = playerHit.GetComponent<PlayerChar>();
					if(_playerController.currentState != "Knockedback")
					{
						_playerController.knockPower = normHorSpeed;
						_playerController.currentState = "Knockedback";
					}
				}
			}
		}
	}
#endregion



#region Private methods
	public bool CheckSides()
	{
		Globals.Instance.setRaycastOrigins(origins, box, skinWidth, trans);
		var isGoingRight = trans.localScale.x > 0;
		var rayDirection = isGoingRight ? Vector2.right : -Vector2.right;
		var initialRayOrigin = isGoingRight ? origins.bottomRight : origins.bottomLeft;
		var rayDistance = .1f;
		for(int i = 0; i < Globals.Instance.totalHorizontalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x, initialRayOrigin.y + i * Globals.Instance.horRayDist);
			m_rayhit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(m_rayhit)
				return true;
		}
		return false;
	}
	
	public bool CheckVertical()
	{
		Globals.Instance.setRaycastOrigins(origins, box, skinWidth, trans);
		var isDashingUp = currentState == "UpDash";
		var rayDirection = isDashingUp ? Vector2.up: -Vector2.up;
		var initialRayOrigin = isDashingUp ? origins.topLeft : origins.bottomLeft;
		var rayDistance = 1f;
		for(int i = 0; i < Globals.Instance.totalVerticalRays; i++)
		{
			var ray = new Vector2(initialRayOrigin.x + i * Globals.Instance.vertRayDist, initialRayOrigin.y);
			m_rayhit = Physics2D.Raycast(ray, rayDirection, rayDistance, playerMask);
			if(m_rayhit)
				return true;
		}
		return false;
	}
	

#endregion
}
