﻿using UnityEngine;
using System.Collections;

public class ItemManager : MonoBehaviour {

	public GameObject[] items;
	public float itemSpawnDelay;

	private int lastSpawned;
	private float m_Timer;
	private GameObject[] spawns;
	void Start()
	{
		for(int x = 0; x < items.Length; x++)
		{
			items[x] = (GameObject)Instantiate(items[x]);
			items[x].SetActive(false);
			items[x].hideFlags = HideFlags.HideInHierarchy;
		}
		spawns = GameObject.FindGameObjectsWithTag("ItemSpawn");
	}


	void Update()
	{
		m_Timer += Time.deltaTime;
		if(m_Timer >= itemSpawnDelay)
			SpawnItem (Random.Range(0, items.Length));
	}

	private void SpawnItem(int index)
	{
		if(index == lastSpawned)
			index -= index % 2 == 0 ? 1 : -1;
		items[index].SetActive(true);
		items[index].transform.position = spawns[Random.Range(0,spawns.Length)].transform.position;
		m_Timer = 0;
	}
}
