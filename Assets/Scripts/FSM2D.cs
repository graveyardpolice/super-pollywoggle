﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class FSM2DState
{
	public string state; //The name of our state
	public Dictionary<string, Action> methods; //Dictionary containing all of our 0 parameter method delegates
	public Dictionary<string, Action<Collider2D>> triggerMethods; //Dictionary containing all of our Trigger(Collider2D) method delegates
	public Dictionary<string, Action<Collision2D>> collisionMethods; //Dictionary Containing all of our Collision(Collision2D) methods delegates
	public FSM2DState(string stateName, Dictionary<string, Action> baseMethodList, Dictionary<string, Action<Collider2D>> trigMethods, Dictionary<string, Action<Collision2D>> colMethods)
	{
		state = stateName;
		methods = baseMethodList;
		triggerMethods = trigMethods;
		collisionMethods = colMethods;
	}
}

public class FSM2D : MonoBehaviour
{
#region State Machine Delegates
	//These will hold our methods for our current state
	public Action EnterState;
	public Action UpdateState;
	public Action ExitState;
	public Action<Collider2D> OnTriggerEnterState;
	public Action<Collider2D> OnTriggerStayState;
	public Action<Collider2D> OnTriggerExitState;
	public Action<Collision2D> OnCollisionEnterState;
	public Action<Collision2D> OnCollisionStayState;
	public Action<Collision2D> OnCollisionExitState;
#endregion

#region State Machine State Variables
	//These will show our current state and our previous state, they are protected so that only the inheriting class has access to them
	//If you need to access state information from another object/class, use a property to retrieve it from the inheriting class.
	protected FSM2DState curState;
	protected FSM2DState prevState;
	//This will hold all of the states assigned to this instance of the machine
	private Dictionary<string, FSM2DState> allStates;
#endregion

#region FSM Update Method

	void Update()
	{
		PreUpdate();
		if(UpdateState != null)
			UpdateState();
		PostUpdate();
	}
#endregion

#region Overridable Methods (State Independent)
	//These are here to be overriden by the inheriting class for anything needed on a constant basis
	//The pre update will run before the states update method, and the post update will run after the states update method
	protected virtual void PreUpdate(){}
	protected virtual void PostUpdate(){}
	//All of these are set to run before the states collision/trigger methods are called
	protected virtual void ConstTriggerEnter2D(Collider2D col){}
	protected virtual void ConstTriggerStay2D(Collider2D col){}
	protected virtual void ConstTriggerExit2D(Collider2D col){}
	protected virtual void ConstCollisionEnter2D(Collision2D col){}
	protected virtual void ConstCollisionStay2D(Collision2D col){}
	protected virtual void ConstCollisionExit2D(Collision2D col){}
#endregion

#region FSM Collision Methods
	void OnTriggerEnter2D(Collider2D col)
	{
		ConstTriggerEnter2D(col);
		if(OnTriggerEnterState != null)
			OnTriggerEnterState(col);
	}

	void OnTriggerStay2D(Collider2D col)
	{
		ConstTriggerStay2D(col);
		if(OnTriggerStayState != null)
			OnTriggerStayState(col);
	}

	void OnTriggerExit2D(Collider2D col)
	{
		ConstTriggerExit2D(col);
		if(OnTriggerExitState != null)
			OnTriggerExitState(col);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		ConstCollisionEnter2D(col);
		if(OnCollisionEnterState != null)
			OnCollisionEnterState(col);
	}

	void OnCollisionStay2D(Collision2D col)
	{
		ConstCollisionStay2D(col);
		if(OnCollisionStayState != null)
			OnCollisionStayState(col);
	}

	void OnCollisionExit2D(Collision2D col)
	{
		ConstCollisionExit2D(col);
		if(OnCollisionExitState != null)
			OnCollisionExitState(col);
	}
#endregion

#region Protected State Machine Methods
	protected void SetUpState(string stateToSetup)
	{
		if(allStates == null)
			allStates = new Dictionary<string, FSM2DState>();
		if(!allStates.ContainsKey("stateToSetup"))
		{
			Dictionary<string, Action> baseList = new Dictionary<string, Action>();
			Dictionary<string, Action<Collider2D>> trigList = new Dictionary<string, Action<Collider2D>>();
			Dictionary<string, Action<Collision2D>> colList = new Dictionary<string, Action<Collision2D>>();
			baseList.Add("Enter", CreateDelegate<Action>(stateToSetup, "Enter", DoNothing));
			baseList.Add("Update", CreateDelegate<Action>(stateToSetup, "Update", DoNothing));
			baseList.Add("Exit", CreateDelegate<Action>(stateToSetup, "Exit", DoNothing));
			trigList.Add("TriggerEnter", CreateDelegate<Action<Collider2D>>(stateToSetup, "TriggerEnter", DoNothingTriggers));
			trigList.Add("TriggerStay", CreateDelegate<Action<Collider2D>>(stateToSetup, "TriggerStay", DoNothingTriggers));
			trigList.Add("TriggerExit", CreateDelegate<Action<Collider2D>>(stateToSetup, "TriggerExit", DoNothingTriggers));
			colList.Add("CollisionEnter", CreateDelegate<Action<Collision2D>>(stateToSetup, "CollisionEnter", DoNothingCollision));
			colList.Add("CollisionStay", CreateDelegate<Action<Collision2D>>(stateToSetup, "CollisionStay", DoNothingCollision));
			colList.Add("CollisionExit", CreateDelegate<Action<Collision2D>>(stateToSetup, "CollisionExit", DoNothingCollision));
			FSM2DState temp = new FSM2DState(stateToSetup, baseList, trigList, colList);
			allStates.Add(stateToSetup, temp);
		}
	}

	protected void SwitchState(string stateName)
	{
		FSM2DState state = null;
		if(allStates.TryGetValue(stateName, out state))
		{
			if(state != null)
			{
				if(ExitState != null)
					ExitState();
				prevState = curState;
				curState = state;
				var baseDict = state.methods;
				var trigDict = state.triggerMethods;
				var colDict = state.collisionMethods;
				foreach(string key in baseDict.Keys)
				{
					Action a = null;
					baseDict.TryGetValue(key, out a);
					if(a != null)
						AssignDelegates(key, a);
					
				}
				foreach(string key in trigDict.Keys)
				{
					Action<Collider2D> a = null;
					trigDict.TryGetValue(key, out a);
					if(a != null)
						AssignDelegates(key, a);
				}
				foreach(string key in colDict.Keys)
				{
					Action<Collision2D> a = null;
					colDict.TryGetValue(key, out a);
					if(a != null)
						AssignDelegates(key, a);
				}
				if(EnterState != null)
					EnterState();
			}
		}
		else
			//If the FSM2D doesnt find the state you are looking for it will throw an error in unity to let you know there is an issue
			Debug.LogError("State: " + stateName + " Doesn't Exist In FSM2D");
	}
#endregion

#region Default Empty Methods
	private void DoNothing(){}
	private void DoNothingTriggers(Collider2D col){}
	private void DoNothingCollision(Collision2D col){}
#endregion

#region Private Methods for State Machine
	private void AssignDelegates(string key, Action a)
	{
		switch(key)
		{
		case "Enter":
			EnterState = a;
			break;
		case "Update":
			UpdateState = a;
			break;
		case "Exit":
			ExitState = a;
			break;
		}
	} 
	private void AssignDelegates(string key, Action<Collider2D> a)
	{
		switch(key)
		{
		case "TriggerEnter":
			OnTriggerEnterState = a;
			break;
		case "TriggerStay":
			OnTriggerStayState = a;
			break;
		case "TriggerExit":
			OnTriggerExitState = a;
			break;
		}
	}
	private void AssignDelegates(string key, Action<Collision2D> a)
	{
		switch(key)
		{
		case "CollisionEnter":
			OnCollisionEnterState = a;
			break;
		case "CollisionStay":
			OnCollisionStayState = a;
			break;
		case "CollisionExit":
			OnCollisionExitState = a;
			break;
		}
	}

	//This is used by the machine internally. It uses reflection to find the methods for each state, and assigns it to the proper places on our state class.
	private T CreateDelegate<T>(string stateName,string methodRoot, T Default) where T : class
	{
		var mtd = GetType().GetMethod(stateName + "_" + methodRoot, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public 
		                              | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.InvokeMethod);
		if(mtd != null)
		{
			return Delegate.CreateDelegate(typeof(T), this, mtd) as T;
		}
		else
			return Default;
	}

	#endregion
}
